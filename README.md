
Implementation of various 2D cellular automata for the [Raspberry Pi][1]'s
[Sense Hat][2]. Currently included:

- [Conway's Game of Life][3]
- [cyclic cellular automata][4]

[1]: https://www.raspberrypi.org/
[2]: https://www.raspberrypi.org/products/sense-hat/
[3]: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
[4]: https://en.wikipedia.org/wiki/Cyclic_cellular_automaton


Authors
-------

    Copyright (C) 2016 Stefano Zacchiroli <zack@upsilon.cc>


License
-------

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
