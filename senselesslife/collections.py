
import collections


class LossyQueue():
    """limited size queue that will discard old-elements to make room for new ones
    when the maximum size is reached

    """

    def __init__(self, maxsize):
        self.q = collections.deque()
        self.maxsize = maxsize

    def __eq__(self, other):
        return self.q == other.q

    def __len__(self):
        return len(self.q)

    def __repr__(self):
        return repr(self.q)

    def __iter__(self):
        return iter(self.q)

    def put(self, item):
        if len(self) == self.maxsize:  # max size reached, make room
            self.get()
        self.q.append(item)

    def get(self, *args, **kwargs):
        return self.q.popleft()
