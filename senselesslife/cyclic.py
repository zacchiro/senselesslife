
import random
import string

from senselesslife.automata import CellularAutomaton2D


class Cyclic(CellularAutomaton2D):
    """2D cyclic cellular automaton

    Cells contain integers. Randomization is a uniform distribution over that
    space. Cells remain unchanged until some of their neighbors have a value
    that (in modulo arithmetic) is one unit larger (n+1) than the current value
    (n). When that happens, cell value becomes n+1.

    Each cell is textually represented as a printable, non-whitespace ASCII
    character.

    See: https://en.wikipedia.org/wiki/Cyclic_cellular_automaton

    """

    DEFAULT_CELL_CARD = 12
    CHARS_DB = ' ' + string.digits + string.ascii_letters + string.punctuation

    def __init__(self, cell_card=DEFAULT_CELL_CARD, *args, **kwargs):
        """initialize a cyclic cellular automate whose cells contain integer in the
        range [0, cell_card - 1]

        cell_card defaults to the DEFAULT_CELL_CARD class attribute

        """
        self.cell_card = cell_card
        self.cell_min = 0
        self.cell_max = cell_card - 1

        if not (self.cell_max < len(self.CHARS_DB)):
            raise ValueError('cannot textually represent %d-bit integers'
                             % self.BITS)

        super().__init__(*args, **kwargs)

    def default_cell(self):
        """cells will be 0 by default"""
        return self.cell_min

    def random_cell(self):
        """random cells are uniformly distributed between [0, cell_card - 1]

        """
        return random.randint(self.cell_min, self.cell_max)

    @classmethod
    def pp_cell(cls, cell):
        return cls.CHARS_DB[cell]

    @classmethod
    def parse_cell(cls, cell):
        try:
            return cls.CHARS_DB.index(cell)
        except ValueError:
            raise ValueError('invalid cell: "%s"' % cell)

    @classmethod
    def parse(cls, s):  # TODO identical to GameOfLife.parse()
        rows = s.rstrip(cls.row_sep_str).split(cls.row_sep_str)
        if not rows:
            raise ValueError('empty board')
        if len(set(map(len, rows))) > 1:
            raise ValueError('non rectangular board')

        board = cls(width=len(rows[0]), height=len(rows), randomize=False)
        for (y, row) in enumerate(reversed(rows)):  # first row is last
            for (x, cell) in enumerate(row):
                board[(x, y)] = cls.parse_cell(cell)

        return board

    def nextgen(self):
        newboard = self.copy(self)

        for (coord, cell) in self.cells():
            successor = (cell + 1) % self.cell_card
            if successor in self.moore_neighbors(coord):
                # at least one successor cell value found among neighbors
                newboard[coord] = successor

        return newboard
