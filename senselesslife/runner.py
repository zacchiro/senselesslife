
import time

from sense_hat import SenseHat

from senselesslife.collections import LossyQueue


DEFAULT_REFRESH = 0.5  # seconds
LOOP_TOLERANCE = 8  # iterations to wait before aborting a loop
SCROLL_SPEED = 0.03


def show_message(hat, msg):
    """show a text message on the Sense hat"""
    hat.show_message(msg, scroll_speed=SCROLL_SPEED)


def mainloop(mk_board, mk_publisher, refresh=DEFAULT_REFRESH):
    """mainloop for running automata game on Pi's Sense Hat

    - mk_board: callable without arguments that will create the automaton to
      run
    - mk_publisher: callable with board and hat kwargs that will create the
      board publisher
    - refresh: time between refreshes (default: DEFAULT_REFRESH)

    """
    hat = SenseHat()

    try:
        while True:
            board = mk_board()
            publisher = mk_publisher(board=board, hat=hat)
            history = LossyQueue(maxsize=10)  # board hist. for loop detection
            countdown = None  # when not None, countdown to finish

            def new_board():
                nonlocal countdown

                publisher.publish(board)
                history.put(board)
                time.sleep(refresh)
                if countdown is not None:
                    countdown -= 1

            new_board()
            for board in iter(board):
                if countdown == 0:
                    show_message(hat, 'loop')
                    break
                if countdown is None and board in history:
                    # loop detected, start countdown
                    countdown = LOOP_TOLERANCE + 1
                new_board()

    except KeyboardInterrupt:
        pass
