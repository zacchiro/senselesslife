
import random

from senselesslife.automata import CellularAutomaton2D


class GameOfLife(CellularAutomaton2D):
    """Conway's Game of Life cellular automata

    Each cell is a boolean:
    - True  -> alive (textual representation: 'X')
    - False -> dead ('.')

    Initial board randomization is 50/50 alive/dead.

    Next generation is computed following Conway's rules (see:
    https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) using Moore's
    neighborood.

    """

    # string representations of alive/dead cells
    alive_cell_str = 'X'
    dead_cell_str = '.'

    def default_cell(self):
        """cells will be dead by default"""
        return False

    def random_cell(self):
        """random cells have 50/50 probability of being alive/dead"""
        return random.choice([True, False])

    @classmethod
    def pp_cell(cls, cell):
        return cls.alive_cell_str if cell else cls.dead_cell_str

    @classmethod
    def parse_cell(cls, cell):
        if cell == cls.alive_cell_str:
            return True
        elif cell == cls.dead_cell_str:
            return False
        else:
            raise ValueError('invalid cell: ' + cell)

    @classmethod
    def parse(cls, s):
        rows = s.rstrip(cls.row_sep_str).split(cls.row_sep_str)
        if not rows:
            raise ValueError('empty board')
        if len(set(map(len, rows))) > 1:
            raise ValueError('non rectangular board')

        board = cls(width=len(rows[0]), height=len(rows), randomize=False)
        for (y, row) in enumerate(reversed(rows)):  # first row is last
            for (x, cell) in enumerate(row):
                board[(x, y)] = cls.parse_cell(cell)

        return board

    def nextgen(self):
        newboard = self.copy(self)

        # Conway's Game of Life rules
        for (coord, cell) in self.cells():
            alive_neighbs = list(self.moore_neighbors(coord)).count(True)
            if cell:  # cell is alive
                if alive_neighbs < 2 or alive_neighbs > 3:
                    # under-/over-population -> death
                    new_cell = False
                else:  # 2 or 3 alive neighbors -> stay alive
                    new_cell = True
            else:  # cell is dead
                if alive_neighbs == 3:  # reproduction -> birth
                    new_cell = True
                else:  # no change -> stay dead
                    new_cell = False

            newboard[coord] = new_cell

        return newboard
