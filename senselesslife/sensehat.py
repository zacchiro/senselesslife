
from abc import ABCMeta, abstractmethod
from functools import lru_cache

from senselesslife import color


PIXEL_ON = [255, 255, 255]
PIXEL_OFF = [0, 0, 0]


def fade_out(pixel, progress):
    """fade out a pixel, based on a [0,1] progress indication

    0 is minimum progress (no fading); 1 maximum progress (complete fading)

    """
    fade = 1 - progress
    return map(lambda i: int(float(i) * fade), pixel)


class BoardPublisher(metaclass=ABCMeta):
    """publisher of board statuses to Sense Hat RGB LED matrix"""

    def __init__(self, board=None, hat=None):
        """initialize a board publisher

        - hat: default target sense_hat.SenseHat where to publish
        - board: board to be used to extract global board parameters

        """
        self._board = board
        self._hat = hat

    @property
    def board(self):
        """board used to look up global parameters"""
        return self._board

    @board.setter
    def board(self, board):
        """set board used to look up global parameters"""
        self._board = board

    @property
    def hat(self):
        """default hat target"""
        return self._hat

    @hat.setter
    def hat(self, hat):
        """set default hat target"""
        self._hat = hat

    @abstractmethod
    def render_cell(self, cell):
        """render a single cell to an RGB pixel

        global board parameters are looked up using the board property

        """
        pass

    def render(self, board):
        """render a board to a 2D pixel matrix suitable for SenseHat.set_pixels

        See https://pythonhosted.org/sense-hat/api/ for reference

        """
        # pre-allocate linearized pixel matrix
        pixels = [None] * board.height * board.width

        for ((x, y), cell) in board.cells():
            y = (board.height - 1) - y  # bottom row is last -> invert Y axis
            pos = y * board.width + x  # map 2D coordinates to 1D
            pixels[pos] = self.render_cell(cell)

        return pixels

    def publish(self, board, hat=None):
        """publish the current state of the board on the hat led matrix

        if target hat is not given as an argument, the default target set in
        the hat property will be used

        """
        if not hat:
            hat = self.hat
        hat.set_pixels(self.render(board))


class OnOffBoolPublisher(BoardPublisher):
    """board publishers for boolean cells"""

    def __init__(self, board=None, hat=None,
                 alive_pixel=PIXEL_ON, dead_pixel=PIXEL_OFF):
        """initialize a boolean published that uses alive_pixel/dead_pixel for
        cells that are alive/dead

        """
        super().__init__(board, hat)

        self.alive_pixel = alive_pixel
        self.dead_pixel = dead_pixel

    def render_cell(self, cell):
        """render a boolean cell to a completely on or completely off pixel"""
        return self.alive_pixel if cell else self.dead_pixel


class ColorIntPublisher(BoardPublisher):
    """board publisher that uses colors to render integer cell values"""

    @property
    @lru_cache(maxsize=1)
    def colors(self):
        """list of colors suitable for the given board

        each color in the list is a triple (r, g, b)
        """
        return list(color.wheel(self.board.cell_max + 1))

    def render_cell(self, cell):
        """render a boolean cell to a point in the RGB color spectrum

        board argument is required to extract cell limits

        """
        # convert (r,g,b) to [r,g,b], just in case, because that's what
        # set_pixels expects
        return list(self.colors[cell])
