
from unittest import TestCase
from nose.tools import istest

from senselesslife.automata import Coord  # SUT


class TestCoordinateStructure(TestCase):

    @istest
    def invalid_quadrant(self):
        self.assertRaises(ValueError, lambda: Coord(-1, 0))
        self.assertRaises(ValueError, lambda: Coord(0, -7))
        self.assertRaises(ValueError, lambda: Coord(-12, -19))

    @istest
    def invalid_quadrant_wrap(self):
        self.assertTrue(Coord(-1, 0, width=3))
        self.assertTrue(Coord(0, -7, height=4))
        self.assertTrue(Coord(-12, -19, height=30, width=40))

    @istest
    def comparison(self):
        self.assertEqual(Coord(1, 2), Coord(1, 2))
        self.assertEqual(hash(Coord(1, 2)), hash(Coord(1, 2)))

    @istest
    def pretty_print(self):
        self.assertEqual('(1,2)', str(Coord(1, 2)))


class TestUnboundCoordinate(TestCase):

    def setUp(self):
        self.coord = Coord(17, 42)

    @istest
    def north(self):
        self.assertEqual(Coord(17, 43), self.coord.n())

    @istest
    def east(self):
        self.assertEqual(Coord(18, 42), self.coord.e())

    @istest
    def south(self):
        self.assertEqual(Coord(17, 41), self.coord.s())

    @istest
    def west(self):
        self.assertEqual(Coord(16, 42), self.coord.w())

    @istest
    def north_east(self):
        self.assertEqual(Coord(18, 43), self.coord.ne())

    @istest
    def south_east(self):
        self.assertEqual(Coord(18, 41), self.coord.se())

    @istest
    def south_west(self):
        self.assertEqual(Coord(16, 41), self.coord.sw())

    @istest
    def north_west(self):
        self.assertEqual(Coord(16, 43), self.coord.nw())

    @istest
    def quadrant_boundaries(self):
        self.assertRaises(ValueError, lambda: Coord(0, 0).s())
        self.assertRaises(ValueError, lambda: Coord(0, 0).w())


class TestBoundCoordinate(TestCase):

    def setUp(self):
        self.coord = Coord(2, 3, width=3, height=4)

    @istest
    def wrap_north(self):
        self.assertEqual(Coord(2, 0), self.coord.n())

    @istest
    def wrap_east(self):
        self.assertEqual(Coord(0, 3), self.coord.e())

    @istest
    def wrap_north_east(self):
        self.assertEqual(Coord(0, 0), self.coord.ne())
