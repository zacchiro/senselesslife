
from itertools import islice
from unittest import TestCase
from nose.tools import istest

from senselesslife.gameoflife import GameOfLife  # SUT


class StillLives(TestCase):

    @istest
    def block(self):
        b = GameOfLife.parse('....\n' +
                             '.XX.\n' +
                             '.XX.\n' +
                             '....\n')
        self.assertEqual(b, b.nextgen())

    @istest
    def beehive(self):
        b = GameOfLife.parse('......\n' +
                             '..XX..\n' +
                             '.X..X.\n' +
                             '..XX..\n' +
                             '......\n')
        self.assertEqual(b, b.nextgen())

    @istest
    def loaf(self):
        b = GameOfLife.parse('......\n' +
                             '..XX..\n' +
                             '.X..X.\n' +
                             '..X.X.\n' +
                             '...X..\n' +
                             '......\n')
        self.assertEqual(b, b.nextgen())

    @istest
    def boat(self):
        b = GameOfLife.parse('.....\n' +
                             '.XX..\n' +
                             '.X.X.\n' +
                             '..X..\n' +
                             '.....\n')
        self.assertEqual(b, b.nextgen())


class Oscillators(TestCase):

    @istest
    def blinker(self):
        b1 = GameOfLife.parse('.....\n' +
                              '.....\n' +
                              '.XXX.\n' +
                              '.....\n' +
                              '.....\n')
        b2 = GameOfLife.parse('.....\n' +
                              '..X..\n' +
                              '..X..\n' +
                              '..X..\n' +
                              '.....\n')
        self.assertEqual(b2, b1.nextgen())
        self.assertEqual(b1, b2.nextgen())

    @istest
    def toad(self):
        b1 = GameOfLife.parse('......\n' +
                              '...X..\n' +
                              '.X..X.\n' +
                              '.X..X.\n' +
                              '..X...\n' +
                              '......\n')
        b2 = GameOfLife.parse('......\n' +
                              '......\n' +
                              '..XXX.\n' +
                              '.XXX..\n' +
                              '......\n' +
                              '......\n')
        self.assertEqual(b2, b1.nextgen())
        self.assertEqual(b1, b2.nextgen())

    @istest
    def beacon(self):
        b1 = GameOfLife.parse('......\n' +
                              '.XX...\n' +
                              '.XX...\n' +
                              '...XX.\n' +
                              '...XX.\n' +
                              '......\n')
        b2 = GameOfLife.parse('......\n' +
                              '.XX...\n' +
                              '.X....\n' +
                              '....X.\n' +
                              '...XX.\n' +
                              '......\n')
        self.assertEqual(b2, b1.nextgen())
        self.assertEqual(b1, b2.nextgen())


class Spaceships(TestCase):

    @staticmethod
    def nth(iterable, n):
        return next(islice(iterable, n, None))

    @istest
    def glider(self):
        b1 = GameOfLife.parse('......\n' +
                              '..X...\n' +
                              '...X..\n' +
                              '.XXX..\n' +
                              '......\n')
        b4 = GameOfLife.parse('......\n' +
                              '......\n' +
                              '...X..\n' +
                              '....X.\n' +
                              '..XXX.\n')
        self.assertEqual(b4, self.nth(iter(b1), 3))

    @istest
    def lightweight_spaceship(self):
        b1 = GameOfLife.parse('...........\n' +
                              '..X..X.....\n' +
                              '......X....\n' +
                              '..X...X....\n' +
                              '...XXXX....\n' +
                              '...........\n' +
                              '...........\n')
        b4 = GameOfLife.parse('...........\n' +
                              '....X..X...\n' +
                              '........X..\n' +
                              '....X...X..\n' +
                              '.....XXXX..\n' +
                              '...........\n' +
                              '...........\n')
        self.assertEqual(b4, self.nth(iter(b1), 3))
