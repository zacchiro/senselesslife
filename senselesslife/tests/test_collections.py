
from unittest import TestCase
from nose.tools import istest

from senselesslife.collections import LossyQueue  # SUT


class TestLossyQueue(TestCase):

    MAXSIZE = 4

    def setUp(self):
        self.q = LossyQueue(maxsize=self.MAXSIZE)

    @istest
    def not_lossy_before_limit(self):
        for i in range(0, self.MAXSIZE):
            self.q.put(i)
        for i in range(0, self.MAXSIZE):
            self.assertEqual(self.q.get(), i)

    @istest
    def lossy_at_limit(self):
        for i in range(0, self.MAXSIZE + 1):
            self.q.put(i)
        self.assertNotIn(0, self.q)
        self.assertEqual(1, self.q.get())

    @istest
    def membership(self):
        self.q.put(1)
        self.q.put(2)
        self.assertIn(1, self.q)
        self.assertIn(2, self.q)
        self.assertNotIn(3, self.q)
