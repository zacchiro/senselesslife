
import copy

from abc import ABCMeta, abstractmethod, abstractclassmethod


class Coord():

    def __init__(self, x, y, width=None, height=None):
        """create a new first quadrant coordinate, bount to (0,0) on the bottom-left,
        and possibly by (width-1, height-1) on the top-right if given

        note that if boundaries are given, coordinate values will wrap around

        """
        if width is not None:
            x = x % width
        if height is not None:
            y = y % height
        if x < 0 or y < 0:
            raise ValueError('coordinate is not in first quadrant: (%d, %d)'
                             % (x, y))

        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def __eq__(self, other):
        """structural comparison, ignoring boundaries"""
        return (self.x == other.x and self.y == other.y)

    def __hash__(self):
        """hash coordinate values, ignoring boundaries"""
        return hash((self.x, self.y))

    def __str__(self):
        """classical math representation of a coordinate: (x,y)"""
        return '(%d,%d)' % (self.x, self.y)

    def __repr__(self):
        return self.__str__()

    def __iter__(self):
        """iterate over coordinates, allowing to unpack: (x, y) = coord"""
        yield self.x
        yield self.y

    def n(self):
        """north"""
        return Coord(self.x, self.y + 1, self.width, self.height)

    def e(self):
        """east"""
        return Coord(self.x + 1, self.y, self.width, self.height)

    def s(self):
        """south"""
        return Coord(self.x, self.y - 1, self.width, self.height)

    def w(self):
        """west"""
        return Coord(self.x - 1, self.y, self.width, self.height)

    def ne(self):
        """north-east"""
        return self.n().e()

    def se(self):
        """south-east"""
        return self.s().e()

    def sw(self):
        """south-west"""
        return self.s().w()

    def nw(self):
        """north-west"""
        return self.n().w()


class CellularAutomaton2D(metaclass=ABCMeta):

    row_sep_str = '\n'

    def __init__(self, width=8, height=8, randomize=True):
        """create a new board of a given size

        If randomize=True, cells will be randomized using random_cell(). If
        randomize=False, cells will be set to default_cell() value.

        """
        self.width = width
        self.height = height

        self.board = [[self.random_cell() if randomize else self.default_cell()
                       for x in range(0, width)]
                      for y in range(0, height)]

    @abstractmethod
    def default_cell(cls):
        """default cell content, when not otherwise specified"""
        pass

    @abstractmethod
    def random_cell(cls):
        """randomly generated cell content"""
        pass

    @abstractclassmethod
    def pp_cell(cls, cell):
        """pretty print cell content to its string representation

        should round-trip with parse_cell()

        """
        pass

    @abstractclassmethod
    def parse_cell(cls, s):
        """parse a cell content from its string representation

        should round-trip with pp_cell()

        """
        pass

    @abstractclassmethod
    def parse(cls, s):
        """parse a board from its string representation

        round-trips with __str__()

        """
        pass

    @abstractmethod
    def nextgen(self):
        """compute the next generation returning a new automaton whose board captures
        the new status

        """
        pass

    def __str__(self):
        """bidimensional textual matrix representation of the board status

        cell (0,0) is the bottom-left cell, with x growing up, and y growing
        right

        round-trips with parse()

        """
        s = ''
        for row in reversed(self.board):
            for cell in row:
                s += self.pp_cell(cell)
            s += self.row_sep_str
        return s

    @classmethod
    def copy(cls, board):
        """copy constructor"""
        board2 = cls(width=board.width, height=board.height, randomize=False)
        board2.board = copy.deepcopy(board.board)

        return board2

    def __eq__(self, other):
        """structural comparison on board content"""
        return (self.board == other.board)

    def __getitem__(self, coord):
        """returns the content of a cell specified by coordinate"""
        (x, y) = coord
        return self.board[y][x]

    def __setitem__(self, coord, content):
        """given a coordinate and new content, update the cell content"""
        (x, y) = coord
        self.board[y][x] = content

    def coords(self):
        """iterate over valid (x,y) board coordinates"""
        for x in range(0, self.width):
            for y in range(0, self.height):
                yield(Coord(x, y, self.width, self.height))

    def cells(self):
        """iterate over cells, i.e., (coord, content) pairs"""
        for coord in self.coords():
            yield (coord, self[coord])

    def moore_neighbors(self, coord):
        """return the content of Moore neighbor cells of a given cell

        see: https://en.wikipedia.org/wiki/Moore_neighborhood

        start with the northern neighbor, iterating clockwise: N, NE, E, SE,
        S, SW, W, NW

        """
        yield(self[coord.n()])
        yield(self[coord.ne()])
        yield(self[coord.e()])
        yield(self[coord.se()])
        yield(self[coord.s()])
        yield(self[coord.sw()])
        yield(self[coord.w()])
        yield(self[coord.nw()])

    def ext_von_neumann_neighbors(self, coord):
        """return the content of extended Von Nuemann neighbor cells of a given cell

        the order is as follows: N, E, S, W, NN, EE, SS, WW

        """
        yield(self[coord.n()])
        yield(self[coord.e()])
        yield(self[coord.s()])
        yield(self[coord.w()])
        yield(self[coord.n().n()])
        yield(self[coord.e().e()])
        yield(self[coord.s().s()])
        yield(self[coord.w().w()])

    neighbors = moore_neighbors

    def __iter__(self):
        """infinite (!) iterator over future generations

        note: the *current* board status will not be included; the iterator
        starts from the next generation

        """
        gen = self
        while True:
            gen = gen.nextgen()
            yield gen
