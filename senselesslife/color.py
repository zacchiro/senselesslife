
import math


def wheel(n):
    """generate a given number of colors, using 3 sine waves on the RGB spectrum

    each color is returned as a (r, g, b) tuple, where each component is in the
    [0,255] range

    see: http://www.markandey.com/2012/04/how-to-generate-rainbow-colors.html
    for a similar algorithm

    """
    freq = float(5) / n

    for i in range(0, n):
        r = math.sin(freq * i + 0) * 127 + 128
        g = math.sin(freq * i + 1) * 127 + 128
        b = math.sin(freq * i + 3) * 127 + 128
        yield (int(r), int(g), int(b))
