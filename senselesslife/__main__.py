
from __future__ import print_function

import time
import sys

from sense_hat import SenseHat

from senselesslife.gameoflife import GameOfLife
from senselesslife.sensehat import BoolCellPublisher

DEFAULT_ITERATIONS = 100
DELAY = 0.5  # seconds

TEXT_SPEED = 0.05  # the smaller, the faster
# see: https://pythonhosted.org/sense-hat/api/


def show_message(hat, msg):
    hat.show_message(msg, scroll_speed=TEXT_SPEED)


def main(iterations):
    hat = SenseHat()
    board = GameOfLife(width=8, height=8, randomize=True)
    publisher = BoolCellPublisher(hat)

    publisher.publish(board)
    time.sleep(DELAY)

    generations = iter(board)
    for i in range(0, iterations):
        prev_board = board
        board = next(generations)

        # progress = float(i)/(iterations-1)
        publisher.publish(board)

        if board == prev_board:  # trivial loop
            show_message(hat, 'loop...')
            # give a moment to look at the board again, then abort
            publisher.publish(board)
            time.sleep(DELAY * 5)
            break

        time.sleep(DELAY)

    show_message(hat, 'The End')


if __name__ == '__main__':
    try:
        iterations = int(sys.argv[1])
    except IndexError:
        iterations = DEFAULT_ITERATIONS
        main(iterations)
